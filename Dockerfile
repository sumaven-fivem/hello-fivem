# Use an appropriate base image that has the necessary tools for your script
FROM ubuntu:latest

# Set the working directory inside the container
WORKDIR /app

# Copy the script into the container
COPY ./run.sh /app/run.sh

# Make the script executable (if needed)
RUN chmod +x /app/run.sh

EXPOSE 40120/tcp
EXPOSE 30120/tcp
EXPOSE 30120/udp

# Specify the command to run your script
CMD ["./run.sh"]
