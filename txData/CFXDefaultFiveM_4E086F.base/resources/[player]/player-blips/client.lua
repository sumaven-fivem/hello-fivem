Citizen.CreateThread(function()
    while true do
        Citizen.Wait(1000)

        if not DoesEntityExist(PlayerPedId()) then
            return
        end

        local players = GetActivePlayers()

        for _, playerId in ipairs(players) do
            -- local playerName = GetPlayerName(playerId)
            -- print(('Player %s with id %i is in the server'):format(name, playerId))

            createBlip(playerId)
            -- ('%s'):format('text') is same as string.format('%s', 'text)
        end
    end
end)

-- Functions
 function createBlip(otherPlayerId)
    local playerName = GetPlayerName(otherPlayerId)
    local otherPlayerPed = GetPlayerPed(otherPlayerId)

    if otherPlayerPed == PlayerPedId() then
        -- prevent creating blip for the player it-self
        return
    end

    if GetBlipFromEntity(otherPlayerPed) ~= 0 then
        -- prevent recreating new blip, because
        -- print("blip exist for current other played ped entity")
        return
    end

    -- Create a blip
    local blip = AddBlipForEntity(otherPlayerPed)
    SetBlipSprite(blip, 1) -- 280 is the sprite for a pedestrian
    SetBlipColour(blip, 0) -- 2 is the color for green
    SetBlipScale(blip, 0.8)
    SetBlipAsShortRange(blip, true)

    -- Set the blip name
    BeginTextCommandSetBlipName("STRING")
    AddTextComponentString("Player - " .. playerName)
    EndTextCommandSetBlipName(blip)
end
