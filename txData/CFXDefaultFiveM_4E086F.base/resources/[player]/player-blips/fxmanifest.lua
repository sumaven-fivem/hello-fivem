fx_version 'cerulean'
games { 'gta5' };

name 'player-blips'
author 'sumaven'
description ''

lua54 'yes'

client_scripts {
    'client.lua'
}

server_script 'server.lua'
