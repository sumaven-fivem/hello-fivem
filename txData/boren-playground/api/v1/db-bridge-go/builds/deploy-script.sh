#!/bin/bash

# Define your application name and Docker image name
APP_NAME="db-bridge-go"
IMAGE_NAME="db_bridge_go"
CONTAINER_NAME="db_bridge_go"

# Stop and remove the old container if it exists
if [ "$(sudo docker ps -q -f name=$CONTAINER_NAME)" ]; then
    sudo docker stop $CONTAINER_NAME
    sudo docker rm $CONTAINER_NAME
fi

# Remove the old image if it exists
if [ "$(sudo docker images -q $IMAGE_NAME)" ]; then
    sudo docker rmi $IMAGE_NAME
fi

# Pull the latest changes from the repository
# git pull origin main

# Build a new Docker image
sudo docker build -t $IMAGE_NAME:latest .

# Run a new container from the built image
sudo docker run -d --name $CONTAINER_NAME -p 127.0.0.1:8080:8080 $IMAGE_NAME:latest

sudo echo "Deployment completed!"
