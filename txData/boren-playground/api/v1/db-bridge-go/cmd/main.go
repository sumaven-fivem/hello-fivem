package main

import (
	"database/sql"
	"db-bridge-go/internal/handlers"
	"db-bridge-go/internal/repositories"
	"db-bridge-go/internal/repositories/mysql"
	"db-bridge-go/internal/repositories/psql"

	"fmt"
	"log"
	"net/http"
	"net/url"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	_ "github.com/jackc/pgx/v5/stdlib"
)

func main() {
	fmt.Println("🌟 REST API Service 🌟")

	db, err := sql.Open("mysql", "fivem:password@tcp(103.193.176.202:3306)/fivem?parseTime=true")
	if err != nil {
		fmt.Println("❌ Failed to connect to the database mysql: {:?}", err)
		log.Fatal(err)
	}
	defer db.Close()

	dbHost := "103.193.176.202"
	dbPort := "5432"
	dbUser := "postgres"
	dbPass := "password"
	dbName := "fivem"
	connection := fmt.Sprintf("postgres://%s:%s@%s:%s/%s", dbUser, dbPass, dbHost, dbPort, dbName)

	val := url.Values{}
	val.Add("sslmode", "disable")

	dsnPg := fmt.Sprintf("%s?%s", connection, val.Encode())
	dbPg, err := sql.Open(`pgx`, dsnPg)
	if err != nil {
		fmt.Println("❌ Failed to connect to the database postgres: {:?}", err)
		log.Fatal(err)
	}

	fmt.Println(("✅ Connection to the database is successful!"))

	repo := repositories.NewNoteRepository(db)
	missionRepository := psql.NewMissionRepository(dbPg)
	mysqlCommandQueryRepository := mysql.NewCommandQueryRepository(db)
	postgresCommandQueryRepository := psql.NewCommandQueryRepository(dbPg)

	handler := &handlers.Handler{Repo: repo}
	mysqlCommandQueryHandler := handlers.NewQueryHandler(mysqlCommandQueryRepository)
	postgresCommandQueryHandler := handlers.NewQueryHandler(postgresCommandQueryRepository)
	missionHandler := handlers.NewMissionHandler(missionRepository)

	router := mux.NewRouter()
	router.HandleFunc("/notes", handler.CreateNoteHandler).Methods("POST")
	router.HandleFunc("/notes", handler.GetAllNotesHandler).Methods("GET")
	router.HandleFunc("/mysql/query", mysqlCommandQueryHandler.Query).Methods("POST")
	router.HandleFunc("/mysql/command", mysqlCommandQueryHandler.Command).Methods("POST")
	router.HandleFunc("/postgres/query", postgresCommandQueryHandler.Query).Methods("POST")
	router.HandleFunc("/postgres/command", postgresCommandQueryHandler.Command).Methods("POST")
	router.HandleFunc("/missions", missionHandler.GetAllMissionsHandler).Methods("GET")

	fmt.Println("✅ Server listening on port 8080")
	log.Fatal(http.ListenAndServe(":8080", router))
}
