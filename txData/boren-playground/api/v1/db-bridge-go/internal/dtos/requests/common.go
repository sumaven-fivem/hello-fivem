package requests

type QueryRequest struct {
	Query string        `json:"query"`
	Args  []interface{} `json:"args"`
}
