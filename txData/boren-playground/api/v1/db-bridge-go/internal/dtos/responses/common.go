package responses

type HttpResponse struct {
	Data          interface{} `json:"data"`
	Message       string      `json:"message"`
	ExecutionTime string      `json:"execution_time"`
	Error         string      `json:"error,omitempty"`
}

type QueryResponse struct {
	Data          []map[string]interface{} `json:"data,omitempty"`
	Message       string                   `json:"message"`
	ExecutionTime string                   `json:"execution_time"`
	Error         string                   `json:"error,omitempty"`
}
