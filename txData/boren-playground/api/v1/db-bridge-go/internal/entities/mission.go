package entities

type Mission struct {
	ID       string      `json:"id"`
	Name     string      `json:"name"`
	IsActive bool        `json:"is_active"`
	Meta     interface{} `json:"meta"`
	Alias    string      `json:"alias"`
}
