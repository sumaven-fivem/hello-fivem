package handlers

import (
	"db-bridge-go/internal/dtos/requests"
	"db-bridge-go/internal/repositories/mysql"
	"encoding/json"
	"net/http"
)

type CommandQueryHandler interface {
	Query(w http.ResponseWriter, r *http.Request)
	Command(w http.ResponseWriter, r *http.Request)
}

type queryHandler struct {
	Repo mysql.CommandQueryRepository
}

func NewQueryHandler(repo mysql.CommandQueryRepository) CommandQueryHandler {
	return &queryHandler{Repo: repo}
}

func (handler *queryHandler) Query(w http.ResponseWriter, r *http.Request) {
	var req requests.QueryRequest
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return
	}

	data, err := handler.Repo.Query(req.Query, req.Args)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	jsonResponse(w, data, http.StatusOK)
}

func (handler *queryHandler) Command(w http.ResponseWriter, r *http.Request) {
	var req requests.QueryRequest
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return
	}

	data, err := handler.Repo.Command(req.Query, req.Args)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	jsonResponse(w, data, http.StatusOK)
}
