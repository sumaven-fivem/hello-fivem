package handlers

import (
	"db-bridge-go/internal/dtos/responses"
	repositories "db-bridge-go/internal/repositories/psql"
	"fmt"
	"net/http"
	"time"
)

type MissionHandler interface {
	GetAllMissionsHandler(w http.ResponseWriter, r *http.Request)
}

type missionHandler struct {
	Repo repositories.MissionRepository
}

func NewMissionHandler(repo repositories.MissionRepository) MissionHandler {
	return &missionHandler{Repo: repo}
}

func (h *missionHandler) GetAllMissionsHandler(w http.ResponseWriter, r *http.Request) {
	start := time.Now()

	notes, err := h.Repo.GetAll()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	duration := time.Since(start)

	response := responses.HttpResponse{
		Data:          notes,
		Message:       "Success",
		ExecutionTime: fmt.Sprintf("%d ms %d μs", duration.Milliseconds(), duration.Microseconds()),
	}
	jsonResponse(w, response, http.StatusOK)
}
