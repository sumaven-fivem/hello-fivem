package handlers

import (
	"db-bridge-go/internal/dtos/responses"
	"db-bridge-go/internal/entities"
	"db-bridge-go/internal/repositories"
	"encoding/json"
	"fmt"
	"net/http"
	"time"
)

// Handler handles HTTP requests
type Handler struct {
	Repo repositories.Repository
}

// CreateNoteHandler handles POST requests to create a new note
func (h *Handler) CreateNoteHandler(w http.ResponseWriter, r *http.Request) {
	var note entities.Note
	if err := json.NewDecoder(r.Body).Decode(&note); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	start := time.Now()

	id, err := h.Repo.Create(&note)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	duration := time.Since(start)

	response := responses.HttpResponse{
		Data: map[string]interface{}{
			"inserted_id": id,
		},
		Message:       "Data successfully inserted",
		ExecutionTime: fmt.Sprintf("%d ms %d μs", duration.Milliseconds(), duration.Microseconds()),
	}
	jsonResponse(w, response, http.StatusCreated)
}

// GetAllNotesHandler handles GET requests to retrieve all notes
func (h *Handler) GetAllNotesHandler(w http.ResponseWriter, r *http.Request) {
	notes, err := h.Repo.GetAll()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	response := map[string]interface{}{
		"status": "success",
		"data":   notes,
	}
	jsonResponse(w, response, http.StatusOK)
}
