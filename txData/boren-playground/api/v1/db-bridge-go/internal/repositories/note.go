package repositories

import (
	"database/sql"
	"db-bridge-go/internal/entities"
)

// Repository defines the interface for note repository
type Repository interface {
	Create(note *entities.Note) (int64, error)
	GetAll() ([]entities.Note, error)
}

// NoteRepository implements Repository using MySQL database
type NoteRepository struct {
	DB *sql.DB
}

// NewNoteRepository creates a new MySQLRepository
func NewNoteRepository(db *sql.DB) *NoteRepository {
	return &NoteRepository{DB: db}
}

// Create inserts a new note into the database
func (r *NoteRepository) Create(note *entities.Note) (int64, error) {
	query := "INSERT INTO notes (title, content, is_published) VALUES (?, ?, ?)"
	result, err := r.DB.Exec(query, note.Title, note.Content, note.IsPublished)
	if err != nil {
		return 0, err
	}
	return result.LastInsertId()
}

// GetAll retrieves all notes from the database
func (r *NoteRepository) GetAll() ([]entities.Note, error) {
	query := "SELECT id, title, content, is_published, created_at, updated_at FROM notes"
	rows, err := r.DB.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var notes []entities.Note
	for rows.Next() {
		var note entities.Note
		err := rows.Scan(&note.ID, &note.Title, &note.Content, &note.IsPublished, &note.CreatedAt, &note.UpdatedAt)
		if err != nil {
			return nil, err
		}
		notes = append(notes, note)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return notes, nil
}
