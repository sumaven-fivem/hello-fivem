package psql

import (
	"database/sql"
	"db-bridge-go/internal/dtos/responses"
	"fmt"
	"time"
)

type CommandQueryRepository interface {
	Query(query string, args []interface{}) (*responses.QueryResponse, error)
	Command(query string, args []interface{}) (*responses.QueryResponse, error)
}

type commandQueryRepository struct {
	DB *sql.DB
}

func NewCommandQueryRepository(db *sql.DB) CommandQueryRepository {
	return &commandQueryRepository{DB: db}
}

func (repo *commandQueryRepository) Query(query string, args []interface{}) (*responses.QueryResponse, error) {
	start := time.Now()

	rows, err := repo.DB.Query(query, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	columns, err := rows.Columns()
	if err != nil {
		return nil, err
	}

	results := []map[string]interface{}{}
	for rows.Next() {
		columnsData := make([]interface{}, len(columns))
		columnsDataPtrs := make([]interface{}, len(columns))
		for i := range columnsData {
			columnsDataPtrs[i] = &columnsData[i]
		}

		if err := rows.Scan(columnsDataPtrs...); err != nil {
			return nil, err
		}

		row := make(map[string]interface{})
		for i, colName := range columns {
			val := columnsData[i]
			b, ok := val.([]byte)
			if ok {
				val = string(b)
			}
			row[colName] = val
		}
		results = append(results, row)
	}

	duration := time.Since(start)

	resp := responses.QueryResponse{
		Data:          results,
		ExecutionTime: fmt.Sprintf("%d ms %d μs", duration.Milliseconds(), duration.Microseconds()),
	}

	return &resp, nil
}

func (repo *commandQueryRepository) Command(query string, args []interface{}) (*responses.QueryResponse, error) {
	start := time.Now()

	var lastInsertID string

	err := repo.DB.QueryRow(query, args...).Scan(&lastInsertID)
	if err != nil {
		return nil, err
	}

	rowsAffected := 1 // For INSERT, rowsAffected should typically be 1

	duration := time.Since(start)

	// Prepare the response
	resp := responses.QueryResponse{
		Data: []map[string]interface{}{
			{
				"rows_affected":  rowsAffected,
				"last_insert_id": lastInsertID,
			},
		},
		ExecutionTime: fmt.Sprintf("%d ms %d μs", duration.Milliseconds(), duration.Microseconds()),
	}

	return &resp, nil
}
