package psql

import (
	"database/sql"
	"db-bridge-go/internal/entities"
)

type MissionRepository interface {
	GetAll() ([]entities.Mission, error)
}

type missionRepository struct {
	DB *sql.DB
}

func NewMissionRepository(db *sql.DB) MissionRepository {
	return &missionRepository{DB: db}
}

func (r *missionRepository) GetAll() ([]entities.Mission, error) {
	query := "SELECT id, name, is_active, meta, alias FROM missions"
	rows, err := r.DB.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var notes []entities.Mission
	for rows.Next() {
		var note entities.Mission
		err := rows.Scan(&note.ID, &note.Name, &note.IsActive, &note.Meta, &note.Alias)
		if err != nil {
			return nil, err
		}
		notes = append(notes, note)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return notes, nil
}
