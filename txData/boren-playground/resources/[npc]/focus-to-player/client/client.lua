local QBCore = exports['qb-core']:GetCoreObject()

local targetedPedGender = nil

local conversationRadius = 3
local targetedPed = nil


---
--- Functions
--- 

function handleDetarget()
    if DoesEntityExist(lastTargetedPed) then
        -- FreezeEntityPosition(lastTargetedPed, false))
        Citizen.Wait(1000)
        TaskWanderStandard(lastTargetedPed, 10.0, 10)
    end

    lastTargetedPed = nil
    targetedPedGender = nil
end

function raycastFromCamera(flag)
    local coords, normal = GetWorldCoordFromScreenCoord(0.5, 0.5)
    local destination = coords + normal * 10

    local handle = StartShapeTestLosProbe(
        coords.x,
        coords.y,
        coords.z,
        destination.x,
        destination.y,
        destination.z,
        flag,
        PlayerPedId(),
        4
    )

    while true do
        Citizen.Wait(0)

        local retVal, hit, endCoords, surfaceNormal, materialHash, entityHit = GetShapeTestResultIncludingMaterial(handle)

        if retVal ~= 1 then
            return hit, entityHit, endCoords, surfaceNormal, materialHash
        end
    end
end

-- Utility function to generate random coordinates within a radius
function getRandomCoordinates(centerX, centerY, centerZ, minRadius, maxRadius)
    local angle = math.random() * math.pi * 2
    local distance = minRadius + math.random() * (maxRadius - minRadius)
    local x = centerX + distance * math.cos(angle)
    local y = centerY + distance * math.sin(angle)
    local z = centerZ + math.random(-10, 10) -- Some random height variation
    return x, y, z
end

-- Utility function to get random street coordinates within a radius
function getRandomStreetCoordinates(centerX, centerY, centerZ, minRadius, maxRadius)
    local position, heading
    for i = 1, 100 do  -- Try up to 100 times to find a valid street position
        local x, y, z = getRandomCoordinates(centerX, centerY, centerZ, minRadius, maxRadius)
        
        local nodeType = 1 -- nodeType 1 means any path/road
        local found, outPosition, streetHeading = GetClosestVehicleNodeWithHeading(x, y, z, nodeType)
        if found then
            position, heading = outPosition, streetHeading
            break
        end
    end
    
    return position, heading
end



---
--- Threads
---

-- display Marker To TagetedPed
Citizen.CreateThread(function()
    local baseHeight = 1.075
    local amplitude = 0.05
    local period = 1200 -- period of the sine wave in milliseconds
    local startTime = GetGameTimer()

    while true do
        Citizen.Wait(0) -- Render every frame

        if (targetedPed) then
            -- Get the ped's position
            local pedCoords = GetEntityCoords(targetedPed)

            -- Calculate the vertical offset and alpha based on sine wave
            local currentTime = GetGameTimer()
            local elapsedTime = currentTime - startTime
            local sineValue = math.sin((elapsedTime % period) / period * 2 * math.pi)
            local verticalOffset = baseHeight + amplitude * sineValue
            local alpha = 100 + 55 * sineValue -- Alpha will oscillate between 45 and 155

            -- Draw the animated marker above the ped's head
            DrawMarker(
                0, -- Type
                pedCoords.x, pedCoords.y, pedCoords.z + verticalOffset, -- Position
                0, 0, 0, -- Direction
                0, 0, 0, -- Rotation
                0.1, 0.1, 0.1, -- Scale
                0, 0, 0, alpha, -- Color (RGBA)
                false, true, 2, -- BobUpAndDown, FaceCamera, P19 (usually set to 2)
                nil, nil, false -- TextureDict, TextureName, DrawOnEnts
            )
        end
    end
end)

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
        if IsControlJustPressed(0, 19) then
            exports["player-status"]:SetIsOnLeftAltHold(true)
        end
        
        if IsDisabledControlJustReleased(0, 19) then
            exports["player-status"]:SetIsOnLeftAltHold(false)
        end

        local playerEntity                  = PlayerPedId()
        local playerCoordinates             = GetEntityCoords(playerEntity)
        local streetHash, crossStreetHash   = GetStreetNameAtCoord(table.unpack(playerCoordinates))
        local streetName                    = GetStreetNameFromHashKey(streetHash)
        local crossStreetName               = GetStreetNameFromHashKey(crossStreetHash)

        LocalPlayer.state:set('curStreetName', streetName, true)
        LocalPlayer.state:set('curCrossStreetName', crossStreetName, true)
    end
end)

-- Add marker to a specific number of nearest ped to player
Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)

        -- Get the player's coordinates
        local playerPed = PlayerPedId()
        local playerCoords = GetEntityCoords(playerPed)
        local radius = conversationRadius

        -- Find NPCs within the defined radius from the player
        local nearbyPeds = {}
        for _, ped in ipairs(GetGamePool('CPed')) do
            if ped ~= playerPed then
                local pedCoords = GetEntityCoords(ped)
                local distance = #(playerCoords - pedCoords)
                if distance <= radius then
                    table.insert(nearbyPeds, { ped = ped, coords = pedCoords, distance = distance })
                end
            end
        end

        -- Sort nearby peds based on distance
        table.sort(nearbyPeds, function(a, b) return a.distance < b.distance end)

        local numOfNearestToDraw = 1

        -- Draw markers for the three nearest NPCs
        for i = 1, math.min(#nearbyPeds, numOfNearestToDraw) do
            local markerCoords = GetOffsetFromEntityInWorldCoords(nearbyPeds[i].ped, 0, 0, 3)
            DrawMarker(23, markerCoords.x, markerCoords.y, markerCoords.z - 0.9, 0.0, 0.0, 0.0, 0.0, 180.0, 0.0, 1.0, 1.0, 1.0, 255, 255, 255, 75, false, true, 2, nil, nil, false)
        end
    end
end)

-- Focus to target core system
Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)

        local playerEntity = PlayerPedId()
        local playerCoordinates = GetEntityCoords(playerEntity)
        local hit, entityHit, endCoords = raycastFromCamera(15)

        -- entityType 1 is an index for player and npc
        isPlayerOrNpc = GetEntityType(entityHit) == 1

        if isPlayerOrNpc then
            if hit == 1 then
                exports["player-status"]:SetIsFocusing(true)
            else
                exports["player-status"]:SetIsFocusing(false)
            end
        end

        local isOnConversation = exports["player-status"]:IsOnConversation()
        local isOnLeftAltHold = exports["player-status"]:IsOnLeftAltHold()

        if ((isOnLeftAltHold) or isOnConversation) and isPlayerOrNpc then
            if hit == 1 then
                local distance = #(playerCoordinates - endCoords)
    
                if distance < conversationRadius then
                    exports["player-status"]:SetIsOnConversation(true)
                    
                    if isOnLeftAltHold then
                        targetedPed = entityHit
                    end
                    
                    if IsPedMale(targetedPed) then
                        targetedPedGender = 'MALE'
                    else
                        targetedPedGender = 'FEMALE'
                    end
                end
            end
        end

        local isOnLeftAltHold = exports["player-status"]:IsOnLeftAltHold()
        if isOnLeftAltHold then
            exports["player-status"]:SetIsTargeting(true)
        else
            exports["player-status"]:SetIsTargeting(false)
        end
        
        if targetedPed then
            local playerCoords = GetEntityCoords(playerEntity)
            local pedCoords = GetEntityCoords(targetedPed)
            local distance = #(playerCoords - pedCoords)
            if distance > conversationRadius then
                exports["player-status"]:SetIsOnConversation(false)
                targetedPed = nil
            end
        end
    end
end)

-- Focus to target core system
Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)

        local isOnConversation = exports["player-status"]:IsOnConversation()

        if not isOnConversation then
            if lastTargetedPed then
                handleDetarget()
            end

            goto continue
        end

        if isOnConversation then
            if lastTargetedPed and lastTargetedPed ~= targetedPed then
                handleDetarget()
            end

            if not lastTargetedPed then
                ClearPedTasks(targetedPed)
                
                TaskTurnPedToFaceEntity(targetedPed, PlayerPedId(), -1)

                lastTargetedPed = targetedPed
            end
        end

        ::continue::
    end
end)

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(1000)
        for i = 1, 5 do
            Citizen.Wait(200)

            local playerNetworkId = NetworkGetNetworkIdFromEntity(PlayerPedId())

            local playerPed = PlayerPedId()  -- Get the player's ped (player character)
            local center = GetEntityCoords(playerPed)  -- Get the coordinates of the player's ped
            
            local minRadius = 25
            local maxRadius = 100

            -- Generate random coordinates within the radius
            local outPosition, heading = getRandomStreetCoordinates(center.x, center.y, center.z, minRadius, maxRadius)
            
            -- TriggerServerEvent('focus-to-player:spawnVehicle', playerNetworkId, outPosition, heading)
        end

        break;
    end
end)



-- 
-- Events
--

RegisterNetEvent('focus-to-player:playAudioOnPed')
AddEventHandler('focus-to-player:playAudioOnPed', function(data)
    local pedEntity = NetworkGetEntityFromNetworkId(data.pedNetworkId)
    xSound:PlayUrlPos(data.soundId, data.audioUrl, 0.075, GetEntityCoords(pedEntity), false)
end)


RegisterNetEvent('focus-to-player:driveVehicle')
AddEventHandler('focus-to-player:driveVehicle', function(npcNetId, vehicleNetId)
    local npc = NetworkGetEntityFromNetworkId(npcNetId)
    local vehicle = NetworkGetEntityFromNetworkId(vehicleNetId)

    if DoesEntityExist(npc) and DoesEntityExist(vehicle) then
        -- Warp the NPC into the driver seat
        TaskWarpPedIntoVehicle(npc, vehicle, -1)

        -- Wait a moment to ensure NPC is in the vehicle
        Wait(500)

        -- Turn on the vehicle engine
        SetVehicleEngineOn(vehicle, true, true, false)

        -- Assign the NPC a task to drive around randomly
        TaskVehicleDriveWander(npc, vehicle, 15.0, 786603) -- 20.0 is the driving speed, 786603 is the driving style (normal)

        -- -- Set the vehicle and NPC as no longer needed to allow the game to manage them
        -- SetEntityAsNoLongerNeeded(vehicle)
        -- SetEntityAsNoLongerNeeded(npc)

        -- -- Optionally set the models as no longer needed to free memory
        -- SetModelAsNoLongerNeeded(vehicleModel)
        -- SetModelAsNoLongerNeeded(npcModel)
    end
end)


---
--- Callbacks
---

RegisterNUICallback("loadAudioUrl", function(audioUrl, cb)
    if targetedPed ~= nil then
        local sourcePlayerId = NetworkGetNetworkIdFromEntity(PlayerPedId())
        local targetedPedNetworkId = NetworkGetNetworkIdFromEntity(targetedPed)
        
        TriggerServerEvent('focus-to-player:registerAudioToPed', audioUrl, sourcePlayerId, targetedPedNetworkId)
    end
end)


-- 
-- exports
-- 

exports('TargetedPedGender', function()
	return targetedPedGender
end)
