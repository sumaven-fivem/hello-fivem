local QBCore = exports['qb-core']:GetCoreObject()
local xSound = exports['xsound']

local attachedSoundsNPC = {}

local vehicleSpawnQueueCoords = {}



local function onUpdateNPCPositions()
    for pedNetworkId, data in pairs(attachedSoundsNPC) do
        local source = data.source
        local soundId = data.soundId

        local npcEntity = NetworkGetEntityFromNetworkId(pedNetworkId)

        if DoesEntityExist(npcEntity) then
            local npcPos = GetEntityCoords(npcEntity) -- npcPos in vector3
            
            xSound:Position(-1, soundId, npcPos) -- -1 source, means apply it to everyones player in the network
        else
            attachedSoundsNPC[pedNetworkId] = nil

            xSound:Destroy(-1, soundId) -- -1 source, means apply it to everyones player in the network
        end
    end
end

-- Function to check if a location is too close to any player
function isLocationTooCloseToPlayers(pos, minDistance)
    local players = GetPlayers()
    for _, playerId in ipairs(players) do
        local playerPed = GetPlayerPed(playerId)
        local playerCoords = GetEntityCoords(playerPed)
        local distance = #(playerCoords - pos)
        if distance < minDistance then
            return true
        end
    end
    return false
end



-- Listen to NPC positions changes
Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0) -- Listen infinitely
        onUpdateNPCPositions()
    end
end)



RegisterNetEvent('focus-to-player:registerAudioToPed', function(audioUrl, sourcePlayerId, pedNetworkId)
    if pedNetworkId ~= 0 then -- if any ped is targeted then the pedNetworkId should not 0
        local pedEntity = NetworkGetEntityFromNetworkId(pedNetworkId)
        local generatedSoundId = "voice_id_" .. pedNetworkId

        print("Registering audio " .. audioUrl .. " from playerId (source) " .. sourcePlayerId .. " to ped " .. pedNetworkId)

        -- -1 source, means apply it to everyones player in the network
        xSound:PlayUrlPos(-1, generatedSoundId, audioUrl, 0.15, GetEntityCoords(pedEntity), false, {
            onPlayStart = onPlayStart,
            onPlayEnd = onPlayEnd
        })

        local data = {}
        data.source = sourcePlayerId
        data.soundId = generatedSoundId

        attachedSoundsNPC[pedNetworkId] = data
    end
end)

RegisterNetEvent('focus-to-player:spawnVehicle', function(playerNetworkId, pos, heading)
    local playerServerId = source

    local avoidRadius = 15
    if isLocationTooCloseToPlayers(pos, avoidRadius) then
        print("too close to players")
        return
    end

    local playerPed = NetworkGetEntityFromNetworkId(playerNetworkId)

    -- Define the NPC model
    local vehicleModel = GetHashKey("adder")
    local npcModel = GetHashKey("a_m_y_hipster_01")

    print("Spawning: ", vehicleModel, " ", npcModel, " by ", playerServerId)

    local vehicle = CreateVehicle(vehicleModel, pos.x, pos.y, pos.z, heading, true, false)

   -- Wait for the vehicle to be created
    while not DoesEntityExist(vehicle) do
        Wait(1)
    end
    local npc = CreatePedInsideVehicle(vehicle, 4, npcModel, -1, true, false)

    -- Wait for the NPC to be created
    while not DoesEntityExist(npc) do
        Wait(1)
    end

    TriggerClientEvent('focus-to-player:driveVehicle', playerServerId, NetworkGetNetworkIdFromEntity(npc), NetworkGetNetworkIdFromEntity(vehicle))
end)
