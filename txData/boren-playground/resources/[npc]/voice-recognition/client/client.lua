local focusToPlayer = exports['focus-to-player']
local utilities = exports['utilities']

local isOnConversation = nil
local isOnConversationChanged = false

local isOnTargeting = nil
local isOnTargetingChanged = false

--
-- Commands
--



--
-- Threads
-- 

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)

        -- print(exports['player-status']:IsFocusing())

        if isOnTargeting ~= exports['player-status']:IsTargeting() then
            isOnTargeting = exports['player-status']:IsTargeting()
            isOnTargetingChanged = true
        end

        if isOnConversation ~= exports['player-status']:IsOnConversation() then
            isOnConversation = exports['player-status']:IsOnConversation()
            isOnConversationChanged = true
        end

        if isOnTargetingChanged or isOnConversationChanged then
            SendNUIMessage({
                eventName = 'voice-recognition',
                eventData = {
                    targetedPedGender = focusToPlayer:TargetedPedGender(),
                    isTargeting = isOnTargeting,
                    isOnConversation = isOnConversation
                }
            })

            isOnTargetingChanged = false
            isOnConversationChanged = false
        end
    end
end)
