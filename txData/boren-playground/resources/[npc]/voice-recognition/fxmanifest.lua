fx_version 'cerulean'
games { 'gta5' };

name 'voice-recognition'

author 'Jonirulah, modified by sumaven'
description 'Voice Recognition from EvilRP, made open-source by Jonirulah'

ui_page 'html/index.html'
lua54 'yes'

client_scripts {
    'client/client.lua',
    'configuration.lua'
}

files {
    'html/index.html',
    'html/script.js',
    'html/style.css',
}
