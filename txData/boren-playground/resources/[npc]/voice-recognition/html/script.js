let ttsModel = ""
let messages = []
let isTargeting = false;
let isOnConversation = false;
let targetedPedGender = null;

navigator.mediaDevices.getUserMedia({ audio: true }).then(async (stream) => {
    document.getElementById("main-container").style.display = "none";
    
    const myvad = await vad.MicVAD.new({
        onSpeechStart: () => {
            // TODO
            // console.log("on speech recognized")
        },
        onSpeechEnd: async (audioFloat32Array) => {
            // console.log("on speech end")
            // do something with `audio` (Float32Array of audio samples at sample rate 16000)...
            let wavBlob = convertFloat32ToWav(audioFloat32Array, 16000);
            
            // to listen
            // let audioBlobUrl = URL.createObjectURL(wavBlob);
            // let audioElement = new Audio(audioBlobUrl);
            // audioElement.play();          

            if (isOnConversation){
                await PerformReq(wavBlob);
            }
        }
    })
    myvad.start()

    window.addEventListener("message", function(event) {
        const eventName = event.data.eventName
        const eventData = event.data.eventData
        
        if (eventName === "voice-recognition"){
            isTargeting = JSON.parse(eventData.isTargeting)
            isOnConversation = JSON.parse(eventData.isOnConversation)
            targetedPedGender = eventData.targetedPedGender
            
            if (isOnConversation){
                hideIndicator()

                const getRandomItemFromArray = arr => arr[Math.floor(Math.random() * arr.length)].toString();

                if (targetedPedGender == "MALE"){
                    ttsModel = getRandomItemFromArray([
                        "aura-angus-en", 
                        "aura-arcas-en", 
                        "aura-helios-en",
                        "aura-orion-en",
                        "aura-orpheus-en",
                        "aura-perseus-en",
                        "aura-zeus-en"
                    ])
                }

                if (targetedPedGender == "FEMALE"){
                    ttsModel = getRandomItemFromArray([
                        "aura-asteria-en", 
                        "aura-athena-en",
                        "aura-hera-en",
                        "aura-luna-en",
                        "aura-stella-en"
                    ])
                }

                if (messages.length == 0){
                    const system = `
                        You are playing the role of NPC in Grand Theft Auto V and are speaking with the NPC \
                        Response conversationally. Don't let them know that you are a gpt. \
                        Behave like you are a real person inside the game. \
                        Answer them in a short sentences.
                    `;
                    messages.push({
                        role: "system",
                        content: system.replace(/\s+/g, ' ').trim()
                    })
                }
            } else {
                showIndicator()

                if (messages.length != 0){
                    messages = []
                }
            }
        } else {
            // console.log("not a voice-recognition event")
        }
    });
});

function hideIndicator(){
    document.getElementById("main-container").classList.remove("slide-out-bck-center");
    document.getElementById("main-container").classList.add("puff-in-center");
    document.getElementById("recognized_text").innerHTML = "";
    document.getElementById('detected_action').innerHTML = "";
    document.getElementById('mic-icon').setAttribute('fill', 'green');
    document.getElementById("mic-status").innerHTML = "Say something to the NPC...";
    document.getElementById("options").style.display = "none";
    document.getElementById("main-container").style.display = "inline-block";
}

function showIndicator(){
    document.getElementById("main-container").classList.add("slide-out-bck-center");
    document.getElementById("main-container").classList.remove("puff-in-center");
}

async function PerformReq(audioBlob) {
    try {
        // Make SST request
        var sttURL = 'https://api.deepgram.com/v1/listen?smart_format=false&model=base&language=en';
        const sttResponse = await fetch(sttURL, {
            method: 'POST',
            headers: {
                'Authorization': 'Token be76343a464903e107d947bf07076c874938c81a',
                'Content-Type': 'audio/wav'
            },
            body: audioBlob
        });
        const sttData = await sttResponse.json();
        const sttResult = sttData.results.channels[0].alternatives[0].transcript;

        // Print out what the user said
        // console.log("You Said:", JSON.stringify(sttResult))

        messages.push({
            role: "user",
            content: sttResult
        })

        // GPT Context: You can use the below stats to enhance your conversation and personality.
        const gptPayload = {
            messages: messages,
            model: "gemma-7b-it"
        };

        const gpt = await fetch("https://api.groq.com/openai/v1/chat/completions", {
            method: 'POST',
            headers: {
                'Authorization': 'Bearer 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',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(gptPayload)
        });
        const gptData = await gpt.json();
        const gptResult = gptData.choices[0].message.content
        // const gptResult = gptData.candidates[0].content.parts[0].text

        // Print out GPT API response
        // console.log("GPT Answered:", gptResult);

        messages.push({
            role: "assistant",
            content: gptResult
        })


        // Make TTS request
        const ttsResponse = await fetch(`https://api.deepgram.com/v1/speak?model=${ttsModel}`, {
            method: 'POST',
            headers: {
                'Authorization': 'Token be76343a464903e107d947bf07076c874938c81a',
                'Content-Type': 'text/plain'
            },
            body: gptResult
        });
        const audioBlobData = await ttsResponse.blob();
        const audioUrl = URL.createObjectURL(audioBlobData);
        
        $.post("https://focus-to-player/loadAudioUrl", JSON.stringify(audioUrl));
        
    } catch (error) {
        // Handle any errors
        console.error('Error:', error);
    }
}

function convertFloat32ToWav(buffer, sampleRate) {
    let bufferLength = buffer.length;
    let wavBuffer = new ArrayBuffer(44 + bufferLength * 2);
    let view = new DataView(wavBuffer);

    function writeString(view, offset, string) {
        for (let i = 0; i < string.length; i++) {
            view.setUint8(offset + i, string.charCodeAt(i));
        }
    }

    /* RIFF identifier */
    writeString(view, 0, 'RIFF');
    /* file length */
    view.setUint32(4, 36 + bufferLength * 2, true);
    /* RIFF type */
    writeString(view, 8, 'WAVE');
    /* format chunk identifier */
    writeString(view, 12, 'fmt ');
    /* format chunk length */
    view.setUint32(16, 16, true);
    /* sample format (raw) */
    view.setUint16(20, 1, true);
    /* channel count */
    view.setUint16(22, 1, true);
    /* sample rate */
    view.setUint32(24, sampleRate, true);
    /* byte rate (sample rate * block align) */
    view.setUint32(28, sampleRate * 2, true);
    /* block align (channel count * bytes per sample) */
    view.setUint16(32, 2, true);
    /* bits per sample */
    view.setUint16(34, 16, true);
    /* data chunk identifier */
    writeString(view, 36, 'data');
    /* data chunk length */
    view.setUint32(40, bufferLength * 2, true);

    /* PCM samples */
    let offset = 44;
    for (let i = 0; i < bufferLength; i++, offset += 2) {
        let s = Math.max(-1, Math.min(1, buffer[i]));
        view.setInt16(offset, s < 0 ? s * 0x8000 : s * 0x7FFF, true);
    }

    return new Blob([view], { type: 'audio/wav' });
}
