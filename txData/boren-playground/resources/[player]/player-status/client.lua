local isFocusing = false
local isTargeting = false
local isOnConversation = false

local isOnLeftAltHold = false

exports('SetIsFocusing', function(value)
    isFocusing = value
end)

exports('SetIsTargeting', function(value)
    isTargeting = value
end)

exports('SetIsOnConversation', function(value)
    isOnConversation = value
end)

exports('SetIsOnLeftAltHold', function(value)
    isOnLeftAltHold = value
end)


exports('IsFocusing', function()
	return isFocusing
end)

exports('IsTargeting', function()
	return isTargeting
end)

exports('IsOnConversation', function()
    return isOnConversation;
end)

exports('IsOnLeftAltHold', function()
	return isOnLeftAltHold
end)
