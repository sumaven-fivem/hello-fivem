local QBCore = exports['qb-core']:GetCoreObject()
local json = require('json')

local function executeQuery(endpoint, query, args, callback)
    PerformHttpRequest("http://db-bridge-go:8080/postgres/query", function(statusCode, response, headers)
        if statusCode == 200 then
            local body = json.decode(response)
            if body.error then
                print("Error: " .. body.error)
                if callback then
                    callback(nil, body.error)
                end
                return
            end
            if callback then
                callback(body.data, nil, body.execution_time)
            end
        else
            print("Failed to execute query, status code: " .. statusCode)
            if callback then
                callback(nil, "Failed to execute query, status code: " .. statusCode)
            end
        end
    end, 'POST', json.encode({query = query, args = args}), {["Content-Type"] = "application/json"})
end

function MySQLQuery(query, args, callback)
    executeQuery("query", query, args, callback)
end

function MySQLInsert(query, args, callback)
    executeQuery("insert", query, args, callback)
end

-- Example usage
RegisterCommand('execquery', function(source, args, rawCommand)
    if not QBCore.Functions.HasPermission(source, 'admin') then
        return
    end

    local query = table.concat(args, " ")
    MySQLQuery(query, {}, function(results, err, executionTime)
        if err then
            print("Error: " .. err)
        else
            print("Query executed in " .. (executionTime))
            for _, row in ipairs(results) do
                for column, value in pairs(row) do
                    print(column .. ": " .. tostring(value))
                end
                print("---")
            end
        end
    end)
end, false)
