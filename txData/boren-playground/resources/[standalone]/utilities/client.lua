local function displayText(text)
    -- Draw a box
    SetTextFont(0)
    SetTextProportional(1)
    SetTextScale(0.0, 0.25)
    SetTextColour(255, 255, 255, 255)
    SetTextDropShadow(0, 0, 0, 0, 255)
    SetTextEdge(1, 0, 0, 0, 255)
    SetTextDropShadow()
    SetTextOutline()
    SetTextRightJustify(true)  -- Set the text to right justify
    SetTextWrap(0.0, 1.0)
    SetTextEntry("STRING")
    AddTextComponentString(text)
    DrawText(1.0, 0.75)
end

exports('DisplayText', displayText)