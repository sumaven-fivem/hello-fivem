#!/bin/bash

cd api/v1/db-bridge-go

CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o ./builds/db-bridge-go-linux cmd/main.go