#!/bin/bash

# Define variables
REMOTE_USER="reinhardjs"   # Replace with your VPS username
REMOTE_HOST="27.112.79.187"  # Replace with your VPS IP address
REMOTE_DIR="txData/hello-fivem/api/v1/db-bridge-go/builds"  # Replace with the path to your repository on the VPS

# Execute git pull on the remote server
ssh ${REMOTE_USER}@${REMOTE_HOST} "cd ${REMOTE_DIR} && bash deploy-script.sh"
